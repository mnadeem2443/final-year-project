<?php

/**
 * connection class
 */
class conn
{
	public $localhost = 'localhost';
	public $user = 'root';
	public $dbpassword = '';
	public $db = 'my_db';


	function __construct()
	{
		$mysqli = new mysqli
		 				(
		 					$this->localhost,
		 					$this->user,
		 					$this->dbpassword,
		 					$this->db
		 				);
		return $mysqli;
	}

	// 	// // Check connection
	// 	// if ($mysqli -> connect_errno) {
 //  // 		echo "Failed to connect to MySQL: " . $this->mysqli -> connect_error;
 //  // 		exit();
	// 	// }

	// }

}

?>