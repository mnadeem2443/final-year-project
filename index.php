<!DOCTYPE html>
<html>
<head>
	<title>Online Auction Website</title>
	<link rel="stylesheet" href="style/css/bootstrap.min.css">
	<script type="text/javascript" src="style/js/jquery.js"></script>
	<script type="text/javascript" src="style/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
		<div class="col-lg-12">

<?php
require "page/bar/navBar.php";

if (isset($_GET["page"])) {
	switch ($_GET["page"]) {
		case 'login':
			require "page/login.php";
			break;
		case 'register':
			require "page/register.php";
			break;
		case 'check':
			require "page/check.php";
			break;
		case 'login_check':
			require "page/login_check.php";
			break;
		case 'logout':
			require "page/logout.php";
			break;
		case 'profile':
			require "page/profile.php";
			break;
		case 'switch':
			require "page/switch_user.php";
			break;
		case 'change_pass':
			require "page/change_pass.php";
			break;
		case 'new_product':
			require "page/new_product.php";
			break;
		case 'new_product_check':
			require "page/new_product_check.php";
			break;
		case 'product':
			require "page/product.php";
			break;
		case 'new_bid':
			require "page/new_bid.php";
			break;
		case 'reset':
			require "page/reset.php";
			break;
		case 'reset_check':
			require "page/reset_check.php";
			break;
		case 'view_my_products':
			require "page/view_my_products.php";
			break;
		case 'upload_img':
			require "page/upload_img.php";
			break;
		case 'upload':
			require "page/upload.php";
			break;
		case 'close_biding':
			require "page/close_bidding.php";
			break;
		case 'view_my_bids':
			require "page/view_my_bids.php";
			break;
		case 'claim':
			require "page/claim.php";
			break;
		case 'change_addr':
			require "page/change_addr.php";
			break;
		case 'password_reset_request_admin':
			require "page/password_reset_request_admin.php";
			break;
		case 'edit_del_product':
			require "page/edit_del_product.php";
			break;
		case 'edit_del_bid':
			require "page/edit_del_bid.php";
			break;
	}
}

else{

	require "page/home.php";
}



?>
		</div>
</div>

</body>
</html>