<nav class="navbar navbar-expand-lg navbar-light bg-light">
<a class="navbar-brand" href="index.php"><img src="img/logo.png" style="width:30%"><br>Online Auction</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
  <ul class="navbar-nav mr-auto">
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown">
        Women's
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="?page=product&category=Women&subcategory=Regular">Regular</a>
        <a class="dropdown-item" href="?page=product&category=Women&subcategory=Fency">Fency</a>
      </div>
    </li>

    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown">
        Men's
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="?page=product&category=Men&subcategory=Regular">Regular</a>
        <a class="dropdown-item" href="?page=product&category=Men&subcategory=Fency">Fency</a>
      </div>
    </li>

    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown">
        Kids's
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="?page=product&category=Kids&subcategory=Regular">Regular</a>
        <a class="dropdown-item" href="?page=product&category=Kids&subcategory=Fency">Fency</a>
      </div>
    </li>
  
    <form class="form-inline my-2 my-lg-0" action="?page=product" method="GET">
      <input class="form-control mr-sm-2" type="hidden" name="page" value="product">
      <input class="form-control mr-sm-2" type="search" name="product_name" placeholder="Search" aria-label="Search">
      <input class="btn btn-outline-success my-2 my-sm-0" type="submit" value="Search">
    </form>
      <?php session_start();
      if(isset($_SESSION["name"])){
        echo "
        <li class='nav-item'>
        <a class='nav-link' href='./index.php?page=profile'>".$_SESSION["name"]."</a>
        </li>

        <li class='nav-item'>
        <a class='nav-link' href='?page=logout'>Logout</a>
        </li>
        ";

      }
      else {
       ?>
      
      <li class="nav-item">
        <a class="nav-link" href="?page=register">Register</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="?page=login">Login</a>
      </li><?php } 
echo
"<li class='nav-item'>
        <a class='nav-link' href='admin/'>Admin Section</a>
      </li>"

;

      ?>
</ul>
  </div>
</nav>