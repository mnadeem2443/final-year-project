<?php

include "class/user.php";

if
	(
		isset($_POST["email_address"]) &&
		isset($_POST["username"]) &&
		isset($_POST["password"]) &&
		isset($_POST["repassword"]) &&
		isset($_POST["usertype"])
	)
{
	$email_address = $_POST["email_address"];
	$username = $_POST["username"];
	$password = $_POST["password"];
	$repassword = $_POST["repassword"];
	$usertype = $_POST["usertype"];

	$f = new user;
	$f->set_email_address($email_address);
	$f->set_username($username);
	$f->set_password($password);
	$f->set_repassword($repassword);
	$f->set_usertype($usertype);

	$f->create_user();
}
?>