<?php
include "class\user.php"; 

$email_address = $_SESSION['email'];

//fetch connection variables
$data = new conn;

//run connection query useing fetched variables
$mysqli = new mysqli($data->localhost,$data->user,$data->dbpassword,$data->db);

// Check connection
if ($mysqli -> connect_errno) {
  echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
  exit();
}

//select record
$user = $mysqli->query("SELECT * from users where email = '$email_address'")->fetch_array(MYSQLI_ASSOC);
$userid = $user['id'];

//select active products with the current user
$products_header = $mysqli->query("SELECT * from products where userid = '$userid' and status = 'Active'")->fetch_array(MYSQLI_ASSOC);
$products = $mysqli->query("SELECT * from products where userid = '$userid' and status = 'Active'");


if (isset($_POST["delete"])) {
    $id = $_POST["id"];

    $products = $mysqli->query("DELETE FROM products WHERE id = '$id' ");
    if ($products) {
        echo "record deleted <a href='?page=edit_del_product'>Goto Edit/Delete products</a>";
    }
}
if (isset($_POST["edit_now"])) {
    $id = $_POST["id"];
    $product_name = $_POST["product_name"];
    $bid_price = $_POST["bid_price"];
    $ClosingDate = $_POST["ClosingDate"];
    $category = $_POST["category"];
    $subcategory = $_POST["subcategory"];

    $products = $mysqli->query("UPDATE products SET product_name = '$product_name', bid_price = '$bid_price', ClosingDate = '$ClosingDate', category = '$category', subcategory = '$subcategory' WHERE id = $id");
    if ($products) {
        echo "record Updated <a href='?page=edit_del_product'>Goto Edit/Delete products</a>";
    }
}
else if (isset($_POST["edit"])) {

    echo "

<h1>Edit Product</h1>                        
                        <form action='' method='POST'>
                            <div class='form-group row'>
                                <label for='product_name' class='col-md-4 col-form-label text-md-right'>Product Name</label>
                                <div class='col-md-6'>
                                    <input type='text' id='product_name' class='form-control' name='product_name' value='".$products_header['product_name']."' autofocus required>
                                    <input value='".$products_header['id']."' name='id' type='hidden'>
                                </div>
                            </div>

                            <div class='form-group row'>
                                <label for='bid_price' class='col-md-4 col-form-label text-md-right'>Bid Price</label>
                                <div class='col-md-6'>
                                    <input type='number' id='bid_price' class='form-control' name='bid_price' value='".$products_header['bid_price']."' required>
                                </div>
                            </div>

                            <div class='form-group row'>
                                <label for='ClosingDate' class='col-md-4 col-form-label text-md-right'>Closing Date</label>
                                <div class='col-md-6'>
                                    <input type='date' id='ClosingDate' class='form-control' name='ClosingDate' value='".$products_header['ClosingDate']."' required>
                                </div>
                            </div>
 

                            <div class='form-group row'>
                                <label for='category' class='col-md-4 col-form-label text-md-right'>Category</label>
                                <div class='col-md-6'>
                                    <select id='category' name='category' class='form-control' required>
                                        <option value='Men' "; if($products_header['category']='Men'){echo "Selected"; } echo ">Men</option>
                                        <option value='Women' "; if($products_header['category']='Women'){echo "Selected";} echo ">Women</option>
                                        <option value='Kids' "; if($products_header['category']='Kids'){echo "Selected";} echo ">Kids</option>
                                    </select>
                                </div>
                            </div>


                            <div class='form-group row'>
                                <label for='subcategory' class='col-md-4 col-form-label text-md-right'>Sub Category</label>
                                <div class='col-md-6'>
                                    <select id='subcategory' name='subcategory' class='form-control' required>
                                        <option value='Regular' "; if($products_header['subcategory']='Regular'){echo "Selected";} echo ">Regular</option>
                                        <option value='Fency' "; if($products_header['subcategory']='Fency'){echo "Selected";} echo ">Fency</option>
                                    </select>
                                </div>
                            </div>

                            <input type='submit' value='Edit_Now' name='edit_now'>
                            
                    </div>
                    </form>

    ";
}

else{
echo "<table class='table'>";
if($products_header > 0){
foreach ($products_header as $key => $value) {
    echo "<th>".$key."</th>";
}

echo "<th>Action</th>";

while ($row = $products->fetch_assoc()) {
    echo "<tr>";
    foreach ($products_header as $key => $value) {
        echo "<td>".$products_header[$key]."</td>";
    }
    echo "<td>
    <form action='' method='post'>
    <input type='hidden' value='".$row['id']."' name='id'>
    <input type='submit' value='edit' name='edit'>
    <input type='submit' value='delete' name='delete'>
    </form>
    </td></tr>";
}}
else{
    echo "No Record Found";
}

echo "</table>";}
?>