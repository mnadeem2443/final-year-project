<div class="jumbotron">
  <h1>Online Auction Website</h1>
  <p>Get your dream products</p>
  <a href="?page=product" class="btn btn-primary">View All Products</a>
  <p></p>


<div class="row">

	<div class="card col-sm">
  	<img class="card-img-top" src="img/kid.jpg" alt="Card image">
  	<div class="card-body">
    	<h4 class="card-title">Kids Products</h4>
    	<p class="card-text">All products related to Kids goes here.</p>
    	<a href="?page=product&category=Kids" class="btn btn-primary">See All</a>
  	</div>
	</div>

	<div class="card col-sm">
  	<img class="card-img-top" src="img/woman.jpg" alt="Card image">
  	<div class="card-body">
    	<h4 class="card-title">Women Products</h4>
    	<p class="card-text">Women products will be available here.</p>
    	<a href="?page=product&category=Women" class="btn btn-primary">See All</a>
  	</div>
	</div>

	<div class="card col-sm">
  	<img class="card-img-top" src="img/man.jpg" alt="Card image">
  	<div class="card-body">
    	<h4 class="card-title">Men Products</h4>
    	<p class="card-text">Men related stuff here</p>
    	<a href="?page=product&category=Men" class="btn btn-primary">See All</a>
  	</div>
	</div>
</div>
</div>