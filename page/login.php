<h1>Login</h1>                        
                        <form action="?page=login_check" method="POST">
                            <div class="form-group row">
                                <label for="email_address" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                                <div class="col-md-6">
                                    <input type="text" id="email_address" class="form-control" name="email_address" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                                <div class="col-md-6">
                                    <input type="password" id="password" class="form-control" name="password" required>
                                </div>
                            </div>

                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a href="?page=reset">Reset Password</a>
                            </div>

                            <?php if( isset( $_GET['error'] ) ) { echo "<div class='col-md-6'> Error: Password or Username not found in database </div>"; } ?>
                    </div>
                    </form>