<?php
include "class\user.php"; 

$email_address = $_SESSION['email'];

//fetch connection variables
$data = new conn;

//run connection query useing fetched variables
$mysqli = new mysqli($data->localhost,$data->user,$data->dbpassword,$data->db);

// Check connection
if ($mysqli -> connect_errno) {
  echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
  exit();
}

//select record
$qry = "SELECT * from users where email = '$email_address'";

$show = $mysqli->query($qry);
$var = $show -> fetch_array(MYSQLI_BOTH);

?>
<h1>Add New Product</h1>                        
                        <form action="?page=new_product_check" method="POST">
                            <div class="form-group row">
                                <label for="product_name" class="col-md-4 col-form-label text-md-right">Product Name</label>
                                <div class="col-md-6">
                                    <input type="text" id="product_name" class="form-control" name="product_name"  autofocus required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="bid_price" class="col-md-4 col-form-label text-md-right">Bid Price</label>
                                <div class="col-md-6">
                                    <input type="number" id="bid_price" class="form-control" name="bid_price" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="ClosingDate" class="col-md-4 col-form-label text-md-right">Closing Date</label>
                                <div class="col-md-6">
                                    <input type="date" id="ClosingDate" class="form-control" name="ClosingDate" required>
                                </div>
                            </div>
 <input type="hidden" id="user" class="form-control" name="user" value= <?php echo $var["id"]; ?> >
 <input type="hidden" id="status" class="form-control" name="status" value="Active" >

                            <div class="form-group row">
                                <label for="category" class="col-md-4 col-form-label text-md-right">Category</label>
                                <div class="col-md-6">
                                    <select id="category" name="category" class="form-control" required>
                                        <option value="Men">Men</option>
                                        <option value="Women">Women</option>
                                        <option value="Kids">Kids</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="subcategory" class="col-md-4 col-form-label text-md-right">Sub Category</label>
                                <div class="col-md-6">
                                    <select id="subcategory" name="subcategory" class="form-control" required>
                                        <option value="Regular">Regular</option>
                                        <option value="Fency">Fency</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            
                    </div>
                    </form>
<?php
if (isset($_GET["error"])) {
    switch ($_GET["error"]) {
        case 'N':
            echo "Product Added Succesfully";
            break;
        case 'Y':
            echo "Error: Product not added";
            break;
        
    }
}

?>