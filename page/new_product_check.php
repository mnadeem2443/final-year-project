<?php
include "class/conn.php";

if(
	isset($_POST["product_name"]) &&
	isset($_POST["bid_price"]) &&
	isset($_POST["user"]) &&
	isset($_POST["status"]) &&
	isset($_POST["category"]) &&
	isset($_POST["subcategory"]) &&
	isset($_POST["ClosingDate"])
){

	$product_name = $_POST["product_name"];
	$bid_price = $_POST["bid_price"];
	$user = $_POST["user"];
	$status = $_POST["status"];
	$category = $_POST["category"];
	$subcategory = $_POST["subcategory"];
	$ClosingDate = $_POST["ClosingDate"];

//fetch connection variables
$data = new conn;

//run connection query useing fetched variables
$mysqli = new mysqli($data->localhost,$data->user,$data->dbpassword,$data->db);

// Check connection
if ($mysqli -> connect_errno) {
  echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
  exit();
}

//switch user record
$qry = "INSERT INTO products (
			product_name,
			bid_price,
			userid,
			category,
			subcategory,
			status,
			ClosingDate
			)
			VALUES(
				'$product_name',
				'$bid_price',
				'$user',
				'$category',
				'$subcategory',
				'$status',
				'$ClosingDate')";

$show = $mysqli->query($qry);

if ($show) {
	header("Location: ./index.php?page=new_product&error=N");
}else{
	echo $show;
}

}

?>