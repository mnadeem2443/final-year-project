<?php
include "class/conn.php";

//fetch connection variables
$data = new conn;

//run connection query useing fetched variables
$mysqli = new mysqli($data->localhost,$data->user,$data->dbpassword,$data->db);

// Check connection
if ($mysqli -> connect_errno) {
  echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
  exit();
}

//select all product record
$qry = "select * from products where status = 'Active'";


if (isset($_GET['category'])) {

	$qry = "select * from products where category = '".$_GET['category']." ' and status = 'Active' ";	
}

if (isset($_GET['product_name'])) {

	$qry = "select * from products where product_name like '".$_GET['product_name']."' and status = 'Active' ";	
}

if (isset($_GET['category']) && isset($_GET['subcategory'])) {

	$qry = "select * from products where category = '".$_GET['category']." ' and subcategory = '".$_GET['subcategory']." ' and status = 'Active' ";	
}

if (isset($_GET['detail']) && isset($_GET['product_id'])) {

	$qry = "select * from products where id = '".$_GET['product_id']." ' and status = 'Active' ";	
}

$result = $mysqli->query($qry);
?>

<table class="table table-hover">
	<th>ID</th>
    <th>Product Image</th>
	<th>Product Name</th>
	<th>Bid Price</th>
	<th>Category</th>
	<th>Sub-Category</th>
	<th>No. of Bid Placed</th>
	<th>Maximum Bid</th>
	<th>Place New Bid</th>

<?php

if ($result->num_rows > 0) {
   
    while($row = $result->fetch_assoc()) {
    	$url = '?page=product&detail=Y&product_id='.$row["id"];
    	$pageurl      = $_SERVER['REQUEST_URI'];

    	//bid data
    	$num_of_bids = $mysqli
    					->query("select * from bid where product_id = '".$row["id"]."'")
    					->num_rows;
    	//current user
    	if ($_SESSION) {
    	$current_user = $mysqli
    					->query("select * from users where email = '".$_SESSION["email"]."'")
    					->fetch_array(MYSQLI_ASSOC);
    				}
        //user of product
        $product_user = $mysqli
                        ->query("select * from products where id = '".$row["id"]."'")
                        ->fetch_array(MYSQLI_ASSOC);

        //user's email against user's id
        $user = $mysqli
                        ->query("select * from users where id = '".$product_user["userid"]."'")
                        ->fetch_array(MYSQLI_ASSOC);



    	//highest bid
    	$highest_bid = $mysqli
    					->query("select MAX(bid_amount) as max_amount from bid where product_id = '".$row["id"]."'")
    					->fetch_array(MYSQLI_ASSOC);

    	//Bid Data
    	$bid = $mysqli
    					->query("select * from bid where product_id = '".$row["id"]."'");

    	//minimum ammount to be entered
    					if($highest_bid["max_amount"] > 0){
    						$min_amount = $highest_bid["max_amount"]+10;
    					}
    					else{
    						$min_amount = $row["bid_price"]+10;	
    					}
    	if (isset($_GET['detail']) && isset($_GET['product_id'])) {
			echo "<h3>Detail</h3>";
			echo "<b>Product ID</b>: ".$row["id"]."<br>";
			echo "<b>Product Name</b>: ".$row["product_name"]."<br>";
			echo "<b>Seller ID</b>: ".$row["userid"]."<br>";
			echo "<b>Starting BID Price</b>: ".$row["bid_price"]."<br>";
			echo "<b>category</b>: ".$row["category"]."/".$row["subcategory"]."<br>";
			echo "<b>Bid Detail:-</b>";
			while($bid_data = $bid->fetch_assoc()){
				echo "<br>User ID: ".$bid_data["userid"]."<br> Bid Amount: ".$bid_data["bid_amount"]."<br>";
			}
		}

        echo
        "<tr><td><a href=$url>" . $row["id"]. "</a></td>
        <td><a href=$url><img class='img-thumbnail' src=" . $row["img"]. "></a></td>
        <td><a href=$url>" . $row["product_name"]. "</a></td>
        <td><a href=$url>" . $row["bid_price"]. "</a></td>
        <td><a href=$url>" . $row["category"]. "</a></td>
        <td><a href=$url>" . $row["subcategory"]. "</a></td>
        <td><a href=$url>$num_of_bids</a></td>
        <td><a href=$url>".$highest_bid["max_amount"]."</a></td>
        <td>";
        if ($_SESSION) {
        
        if ($user["email"] == $_SESSION["email"]) {
            echo "You can not bid on your own product";
        }
        else{
            echo "
        <form action='?page=new_bid' method='Post'>
        <input type='number' name='bid' min=$min_amount value=$min_amount required>
        <input type='hidden' name='current_user' value='".$current_user["id"]. " '>
        <input type='hidden' name='product_id' value='".$row["id"]. " '>
        <input type='hidden' name='url' value='$pageurl' >
        <input type='submit' name='submit' value='Bid Now' class='btn btn-primary' >
        </form>";
        }
        echo "</td></tr>";
    	
        }
    	else{
    		echo "<a href=?page=login>Login to BID</a>";
    	}
    }
   
} else {
    echo "0 results";
}
echo "</table>";
?>