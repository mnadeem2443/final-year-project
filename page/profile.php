<?php 
// session_start();
include "class\user.php"; 

$email_address = $_SESSION['email'];

//fetch connection variables
$data = new conn;

//run connection query useing fetched variables
$mysqli = new mysqli($data->localhost,$data->user,$data->dbpassword,$data->db);

// Check connection
if ($mysqli -> connect_errno) {
  echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
  exit();
}

//select record
$qry = "SELECT * from users where email = '$email_address'";

$show = $mysqli->query($qry);
$var = $show -> fetch_array(MYSQLI_BOTH);

?>
<div class="jumbotron">
  <h1>Profile</h1>
  <h3> <?php echo $var['username']; ?> </h3>


<div class="row">

	<div class="card col-sm">
  	<!-- <img class="card-img-top" src="img/kid.jpg" alt="Card image"> -->
  	<div class="card-body">
    	<h4 class="card-title">User Type</h4>
    	<p class="card-text"><?php echo $var['usertype']; ?></p>
    	<form action="?page=switch" method="POST">
        <input type="hidden" name="usertype" value = 
        <?php 
        
        if($var['usertype'] == "Buyer"){
          echo "Seller"; }
        else{
          echo "Buyer";
        }
        
        ?> >
        <input class="btn btn-primary" type="submit" name="switch" value="Switch"></form>
      <?php
      if($var['usertype'] == "Seller"){
      echo "<br><br><a href='?page=new_product' class='btn btn-primary'>Add New Product</a>";
      echo "<br><br><a href='?page=view_my_products&userid=".$var['id']."' class='btn btn-primary'>View My Products</a>";
      echo "<br><br><a href='?page=edit_del_product' class='btn btn-primary'>Edit/Delete Products</a>";
    }
    else if ($var['usertype'] == "Buyer"){
        echo "<br><br><a href='?page=view_my_bids&userid=".$var['id']."' class='btn btn-primary'>View my All Bids</a>";
        echo "<br><br><a href='?page=edit_del_bid' class='btn btn-primary'>Edit/Delete Bids</a>";
    }
      ?>
    
  	</div>
	</div>

	<div class="card col-sm">
  	<!-- <img class="card-img-top" src="img/woman.jpg" alt="Card image"> -->
  	<div class="card-body">
    	<h4 class="card-title">Email Address</h4>
    	<p class="card-text"><?php echo $var['email']; ?></p>
    	<p>Please Note: In order to change your email address, you have send an email to Admin at admin@example.com</p>
  	</div>
	</div>

	<div class="card col-sm">
  	<!-- <img class="card-img-top" src="img/man.jpg" alt="Card image"> -->
  	<div class="card-body">
    	<h4 class="card-title">Change Password</h4>
    	<form action="?page=change_pass" method="POST">
       <input type="password" name="password" placeholder="Password"> 
       <input type="password" name="repassword" placeholder="Re-Password"> 
       <input type="submit" name="submit" value="Change" class="btn btn-primary"> 
      </form>
  	</div>

  <div class="card col-sm">
    <!-- <img class="card-img-top" src="img/man.jpg" alt="Card image"> -->
    <div class="card-body">
      <h4 class="card-title">Shipping Address</h4>
      <form action="?page=change_addr" method="POST">
       <textarea name="address"><?php echo $var['Shipping_Address']; ?></textarea>
       <input type="submit" name="Chng_addr" value="Change" class="btn btn-primary"> 
      </form>
    </div>
	</div>
</div>
</div>