<h1>Registration</h1>                        
                        <form action="?page=check" method="POST">
                            <div class="form-group row">
                                <label for="email_address" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                                <div class="col-md-6">
                                    <input type="email" id="email_address" class="form-control" name="email_address"  autofocus required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="username" class="col-md-4 col-form-label text-md-right">Full User Name</label>
                                <div class="col-md-6">
                                    <input type="text" id="username" class="form-control" name="username" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                                <div class="col-md-6">
                                    <input type="password" id="password" class="form-control" name="password" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="repassword" class="col-md-4 col-form-label text-md-right">Confirm Password</label>
                                <div class="col-md-6">
                                    <input type="password" id="repassword" class="form-control" name="repassword" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="usertype" class="col-md-4 col-form-label text-md-right">User Type</label>
                                <div class="col-md-6">
                                    <select id="usertype" name="usertype" class="form-control" required>
                                        <option value="Buyer">Buyer</option>
                                        <option value="Seller">Seller</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            
                    </div>
                    </form>
<?php
if (isset($_GET["msg"])) {
    switch ($_GET["msg"]) {
        case 'Y':
            echo "User Added Succesfully";
            break;
        case 'email':
            echo "Email Already Registered";
            break;
        case 'password':
            echo "Password Mismatched";
            break;
        
    }
}


?>