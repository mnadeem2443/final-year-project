<h1>Reset Your Password</h1>                        
                        <form action="?page=password_reset_request_admin" method="POST">
                            <div class="form-group row">
                                <label for="email_address" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                                <div class="col-md-6">
                                    <input type="text" id="email_address" class="form-control" name="email_address" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="new_password" class="col-md-4 col-form-label text-md-right">New Password</label>
                                <div class="col-md-6">
                                    <input type="Password" id="new_password" class="form-control" name="new_password" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="renew_password" class="col-md-4 col-form-label text-md-right">Re New Password</label>
                                <div class="col-md-6">
                                    <input type="Password" id="renew_password" class="form-control" name="renew_password" required autofocus>
                                </div>
                            </div>

                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Reset Password
                                </button>

                            </div>

                            <?php
                            if( isset( $_GET['password_change'] ) ) { echo "<div class='col-md-6'> Note: Password Reset to '12345', Please change it ASAP </div><a href='?page=login'>Goto Login Page</a>"; }
                            if( isset( $_GET['password_missmatched'] ) ) { echo "<div class='col-md-6'> Error: Both Password mismatched please try again"; }
                            if( isset( $_GET['request_sent'] ) ) { echo "<div class='col-md-6'> Note: Password Reset Request Sent to Admin"; }
                             ?>
                    </div>
                    </form>