<?php
include "class/conn.php";

//fetch connection variables
$data = new conn;

//run connection query useing fetched variables
$mysqli = new mysqli($data->localhost,$data->user,$data->dbpassword,$data->db);

$target_dir = "uploads/";
$id = $_POST["id"];
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$new_target_file = $target_dir . time() . "." .$imageFileType;
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $new_target_file)) {

    	$qry = "UPDATE products SET img = '$new_target_file' WHERE id = $id";

		$show = $mysqli -> query($qry);

        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded. Goto <a href= '?page=profile'>Profile</a>";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
?>