<?php
include "class/conn.php";

//fetch connection variables
$data = new conn;

//run connection query useing fetched variables
$mysqli = new mysqli($data->localhost,$data->user,$data->dbpassword,$data->db);

// Check connection
if ($mysqli -> connect_errno) {
  echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
  exit();
}

//select all product record
$qry = "select * from products where status = 'Active'";


if (isset($_GET['category'])) {

    $qry = "select * from products where category = '".$_GET['category']." ' ";   
}

if (isset($_GET['userid'])) {

    $qry = "select * from products where userid = '".$_GET['userid']." '  ";   
}

if (isset($_GET['product_name'])) {

	$qry = "select * from products where product_name like '".$_GET['product_name']."'  ";	
}

if (isset($_GET['category']) && isset($_GET['subcategory'])) {

	$qry = "select * from products where category = '".$_GET['category']." ' and subcategory = '".$_GET['subcategory']." '  ";	
}

if (isset($_GET['detail']) && isset($_GET['product_id'])) {

	$qry = "select * from products where id = '".$_GET['product_id']." '  ";	
}

$result = $mysqli->query($qry);
?>
<caption>My Products</caption>
<table class="table table-hover">
	<th>ID</th>
	<th>Product Name</th>
    <th>Product Image</th>
	<th>Bid Price</th>
	<th>Category</th>
	<th>Sub-Category</th>
	<th>No. of Bid Placed</th>
	<th>Maximum Bid</th>
    <th>Status</th>
    <th>Closing Date</th>
    <th>Action</th>
    <th>Winner User ID</th>

<?php

if ($result->num_rows > 0) {
   
    while($row = $result->fetch_assoc()) {
    	$url = '?page=product&detail=Y&product_id='.$row["id"];
    	$pageurl      = $_SERVER['REQUEST_URI'];

    	//bid data
    	$num_of_bids = $mysqli
    					->query("select * from bid where product_id = '".$row["id"]."'")
    					->num_rows;
    	//current user
    	if ($_SESSION) {
    	$current_user = $mysqli
    					->query("select * from users where email = '".$_SESSION["email"]."'")
    					->fetch_array(MYSQLI_ASSOC);
    				}
        //highest bid
        $highest_bid = $mysqli
                        ->query("select userid, MAX(bid_amount) as max_amount from bid where  product_id = '".$row["id"]."'")
                        ->fetch_array(MYSQLI_ASSOC);
        $maxbid = $highest_bid["max_amount"];

        if ($maxbid > 0 ) {
            $maxbid = $highest_bid["max_amount"];
        }
        else
        {
            $maxbid = 0;
        }

    	//Bid Data
    	$bid = $mysqli
    					->query("select id from bid where product_id = '".$row["id"]."' and bid_amount = $maxbid")
                        ->fetch_array(MYSQLI_ASSOC)
                        ;
        $bid_data = $mysqli
                        ->query("select id from bid where product_id = '".$row["id"]."' and bid_amount = $maxbid");

    	//minimum ammount to be entered
    					if($highest_bid["max_amount"] > 0){
    						$min_amount = $highest_bid["max_amount"]+10;
    					}
    					else{
    						$min_amount = $row["bid_price"]+10;	
    					}
    	if (isset($_GET['detail']) && isset($_GET['product_id'])) {
			echo "<h3>Detail</h3>";
			echo "<b>Product ID</b>: ".$row["id"]."<br>";
			echo "<b>Product Name</b>: ".$row["product_name"]."<br>";
			echo "<b>Seller ID</b>: ".$row["userid"]."<br>";
			echo "<b>Starting BID Price</b>: ".$row["bid_price"]."<br>";
			echo "<b>category</b>: ".$row["category"]."/".$row["subcategory"]."<br>";
			echo "<b>Bid Detail:-</b>";
			while($bid_data = $bid_data->fetch_assoc()){
				echo "<br>User ID: ".$bid_data["userid"]."<br> Bid Amount: ".$bid_data["bid_amount"]."<br>";
			}
		}

        echo
        "<tr>
        <td>" . $row["id"]. "</td>
        <td>";

        if ($row["img"] == Null) {
            echo "<form action='?page=upload_img' method='post' >
            <input type='hidden' name='id' value=". $row['id']." >
            <input type='submit' name='submit' value='Upload Image'>
            </form>";
        }

        echo "<img class='img-thumbnail' src=" . $row["img"]. "></td>
        <td>" . $row["product_name"]. "</td>
        <td>" . $row["bid_price"]. "</td>
        <td>" . $row["category"]. "</td>
        <td>" . $row["subcategory"]. "</td>
        <td>$num_of_bids</td>
        <td>".$highest_bid["max_amount"]."</td>
        <td>".$row["status"]."</td>
        <td>".$row["ClosingDate"]."</td>
        <td>";
        
        $date_expire = $row["ClosingDate"];
        $date = new DateTime($date_expire);
        $now = new DateTime();
        
        if($now >= $date && $row["status"] == 'Active'){
            echo "

            <form action='?page=close_biding' method='post'>
            <input type='hidden' value=".$row['id']." name='prd_id'>
            <input type='hidden' value=".$highest_bid["userid"]." name='winner_id'>
            <input type='hidden' value=".$bid["id"]." name='highest_bid_id'>
            <input type='submit' value='Close Product for Bidding' name='submit'>
            </form>

            ";
        }

        echo "</td>
        <td>".$highest_bid["userid"]."</td>
        </tr>";
    }
   
} else {
    echo "0 results";
}
echo "</table>";
?>